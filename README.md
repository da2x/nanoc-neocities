# nanoc-neocities

This Gem provides a deployer that allows [Nanoc](https://nanoc.ws/),
the open-source static website generator, to deploy to
[Neocities](https://neocities.org/)™; the free and open-source web
hosting service for creative web-native individuals.

## Installation

Either `gem install nanoc-neocities` and `require 'nanoc-neocities'`
from your Nanoc site’s `lib/`.

Or add `nanoc-neocities` to the `nanoc` group of your Gemfile:

```
group :nanoc do
  gem 'nanoc-neocities'
end
```

Available on [RubyGem](https://rubygems.org/gems/nanoc-neocities/).

## Usage

Add a deployment target to your `nanoc.yaml` file and provide the
sitename (without the `.neocities.org` TLD) and your site API key:

```
deploy:
  my_neocities:
    kind: neocities
    sitename: example-site
    api_key: example-api-key
    prune_remote: true
```

You can then run `nanoc deploy my_neocities` from inside your Nanoc
site directory to deploy it to your configured Neocities™ site.

You can find the API key for your Neocities™ site on the Site
settings page on the main Neocities™ website.

---

The project is licensed under the MIT License (see LICENSE).
“Neocities” is a trademark of Neocities Ltd.