# frozen_string_literal: true

# This file os based on Nanoc::Deploying::Deployer::Git
# by Copyright © 2017 Denis Defreyne (MIT License).

require 'neocities'

module Nanoc::Deploying::Deployers
  # A deployer that deploys a site to [Neocities](https://neocities.org/).
  #
  # @example A deployment configuration for Neocities:
  #
  #   deploy:
  #     default:
  #       kind:         neocities
  #       api_key:      1234abcd
  #       sitename:     mysite
  #       prune_remote: true
  #
  class NeocitiesDeployer < ::Nanoc::Deploying::Deployer
    identifier :neocities

    module Errors
      class Generic < ::Nanoc::Error
      end

      class OutputEmptyOrGone < Generic
        def initialize(path)
          super("The directory to deploy, #{path}, does not exist.")
        end
      end

      class AuthenticationFailed < Generic
        def initialize()
          super('Your Neocities API key or sitename failed to authenticate your request.')
        end
      end

      class APIerror < Generic
        def initialize(object)
          super("Neocities API error “#{object}”.")
        end
      end

      class UploadFailed < Generic
        def initialize(path, message)
          super("Failed to upload file #{path} to Neocities: #{message}")
        end
      end

      class DeleteOrphanFailed < Generic
        def initialize(path)
          super("Failed to delete orphaned file #{path} from Neocities.")
        end
      end
    end

    def run
      unless File.exist?(source_path)
        raise Errors::OutputEmptyOrGone.new(source_path)
      end

      api_key  = config.fetch(:api_key,  '')
      sitename = config.fetch(:sitename, '')
      prune    = config.fetch(:prune_remote, true)

      client = Neocities::Client.new({api_key: api_key, sitename: sitename})

      remote_files = client.list
      
      unless remote_files[:result] == 'success'
        if remote_files[:error_type] == 'invalid_auth'
puts remote_files
          raise Errors::AuthenticationFailed
        else
          raise Errors::APIerror.new(remote_files)
        end
      end

      puts "Deploying to Neocities site “#{sitename}”…"

      Dir.chdir(source_path) do
        Dir.glob("**/*") {|path|
          unless File.directory?(path)
            upload = client.upload(path, path)
            if upload[:result] == 'success'
              puts "Uploaded file “#{path}”."
            elsif upload[:error_type] == 'file_exists'
              puts "Existing file “#{path}” is identical. Skipping upload."
            else 
              raise Errors::UploadFailed.new(path, upload[:message] || 'unknown problem')
            end
          end
        }

        puts "Neocities site “#{sitename}” deployed."

        if prune
          remote_files = client.list
          puts "Deleting orphaned files from Neocities site “#{sitename}”…"

          orphans = client.list[:files].map{|file| file[:path]} - Dir.glob("**/*")

          for path in orphans
            puts "Deleting “#{path}”."
            unless client.delete(orphan)[:result] == 'success'
              raise Errors::DeleteOrphanFailed.new(path)
            end
          end
        end

      end
    end
  end
end
