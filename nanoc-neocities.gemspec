Gem::Specification.new do |spec|
  spec.name        = 'nanoc-neocities'
  spec.version     = '0.1.2'
  spec.date        = '2019-04-15'
  spec.summary     = 'Deploy from Nanoc to Neocities.'
  spec.description = 'Deploy your static website built with Nanoc to your Neocities site.'
  spec.author      = 'Daniel Aleksandersen'
  spec.email       = 'code+rubygems@daniel.priv.no'
  spec.homepage    = 'https://rubygems.org/gems/nanoc-neocities/'
  spec.metadata    = {
    'bug_tracker_uri' => 'https://bitbucket.org/da2x/nanoc-neocities/issues',
    'changelog_uri'   => 'https://bitbucket.org/da2x/nanoc-neocities/commits/branch/master',
    'source_code_uri' => 'https://bitbucket.org/da2x/nanoc-neocities/src'
  }
  spec.license     = 'MIT'
  spec.files       = [
    'README.md',
    'LICENSE',
    'lib/nanoc-neocities.rb'
  ]
  spec.required_ruby_version = '~> 2.6'
  spec.add_runtime_dependency 'nanoc',     '~> 4.11',  '>= 4.11.2'
  spec.add_runtime_dependency 'neocities', '~> 0.0.13'
end





